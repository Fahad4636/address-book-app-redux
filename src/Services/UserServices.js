import axios from "axios";
import User from "../Core/User";
import Pagination from "../Core/Pagination";

const SEED = "fahad";
const PER_PAGE = 50;

export default class UserService {
  static async getUsers(
    pageNo,
    nationalities = ["CH", "ES", "FR", "GB"],
    limit = PER_PAGE,
    fields = "name,email,login,picture,location,cell,phone,nat"
  ) {
    const res = await axios.get(
      `https://randomuser.me/api/?results=${limit}&page=${pageNo}&nat=${nationalities}&inc=${fields}&seed=${SEED}`
    );
    return {
      users: res.data.results.map(user => {
        return User.createFromObj(user);
      }),
      paginationInfo: Pagination.createPagination(res.data.info.page)
    };
  }
}
