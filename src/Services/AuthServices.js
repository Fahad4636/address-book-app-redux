import axios from "axios";
export default class AuthService {
  static async registerUser(registerData) {
    const res = await axios.post(
      "http://localhost:4000/api/v1/user/add",
      registerData
    );
    return res.data;
  }
  static async loginUser(loginData) {
    let res = await axios.post(
      "http://localhost:4000/api/v1/user/fetchByEmail",
      loginData
    );
    const { token } = await res.data;
    if (token) {
      localStorage.setItem("jwtToken", token);
    }
    return res.data;
  }

  static logoutUser() {
    localStorage.removeItem("jwtToken");
  }
}
