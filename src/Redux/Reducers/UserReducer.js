import userConstants from "../Constant/User";
import settingsConstants from "../Constant/Settings";
import SearchService from "../../Services/SearchService";

const userSearch = new SearchService([]);
const initialState = {
  isLoading: false,
  users: [],
  nationalities: ["CH", "ES", "FR", "GB"],
  userDetail: null,
  search: "",
  filteredUsers: [],
  paginationInfo: {
    currentPageNo: 0,
    hasMore: () => {
      return true;
    }
  },
  nextUsersBatch: null
};

const userReducer = (state = initialState, action) => {
  if (typeof state === "undefined") {
    return initialState;
  }
  switch (action.type) {
    case userConstants.GET_USERS_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case userConstants.RECEIVE_USERS:
      const userList = state.users;
      const { users, paginationInfo } = action.payload;
      const newUsers = userList.concat(users);
      userSearch.SearchIndex(newUsers);
      let newFilteredList = newUsers;
      if (state.search !== "") {
        newFilteredList = userSearch.SearchByUserName(state.search);
      }
      return {
        ...state,
        isLoading: false,
        users: newUsers,
        paginationInfo,
        filteredUsers: newFilteredList
      };
    case userConstants.INVALID_USERS_LIST:
      return initialState;
    case userConstants.VIEW_USER_DETAIL:
      return {
        ...state,
        userDetail: action.payload
      };
    case userConstants.CLOSE_USER_DETAIL:
      return {
        ...state,
        userDetail: null
      };
    case userConstants.SEARCH_USERS:
      let filteredUsers = state.users;
      const search = action.payload;
      if (search !== "") {
        filteredUsers = userSearch.SearchByUserName(search);
      }
      return {
        ...state,
        search: search,
        filteredUsers
      };
    case userConstants.NEXT_BATCH:
      return {
        ...state,
        nextUsersBatch: action.payload
      };
    case settingsConstants.CHANGE_NATIONALITY:
      return {
        ...state,
        nationalities: action.payload
      };
    default:
      return state;
  }
};
export default userReducer;
