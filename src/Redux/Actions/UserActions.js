import userConstants from "../Constant/User";
import userService from "../../Services/UserServices";

export const getUsersRequest = () => {
  return {
    type: userConstants.GET_USERS_REQUEST
  };
};

export const receiveUsers = usersData => {
  return {
    type: userConstants.RECEIVE_USERS,
    payload: usersData
  };
};

export const invalidUsersList = () => {
  return {
    type: userConstants.INVALID_USERS_LIST
  };
};

export const showUserDetail = data => {
  return {
    type: userConstants.VIEW_USER_DETAIL,
    payload: data
  };
};

export const closeUserDetail = () => {
  return {
    type: userConstants.CLOSE_USER_DETAIL
  };
};

export const searchUser = value => {
  return {
    type: userConstants.SEARCH_USERS,
    payload: value
  };
};

export const nextBatch = nextBatchData => {
  return {
    type: userConstants.NEXT_BATCH,
    payload: nextBatchData
  };
};

export const getUsersData = () => {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const { paginationInfo, nationalities } = state.user;
      dispatch(getUsersRequest());
      let usersDataList = await getNextData(state);
      dispatch(receiveUsers(usersDataList));
      let nextBatchData = await userService.getUsers(
        paginationInfo.currentPageNo + 2,
        nationalities
      );
      dispatch(nextBatch(nextBatchData));
    } catch (e) {
      dispatch(invalidUsersList());
    }
  };
};

const getNextData = async state => {
  const { paginationInfo, nationalities, nextUsersBatch } = state.user;
  if (nextUsersBatch === null) {
    return await userService.getUsers(
      paginationInfo.currentPageNo + 1,
      nationalities
    );
  } else {
    return nextUsersBatch;
  }
};
