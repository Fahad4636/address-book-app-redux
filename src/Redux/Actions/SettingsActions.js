import settingsConstants from "../Constant/Settings";

export const changeNationality = nat => {
  return {
    type: settingsConstants.CHANGE_NATIONALITY,
    payload: nat
  };
};
