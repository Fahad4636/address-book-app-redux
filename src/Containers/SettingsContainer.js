import React, { Component } from "react";
import { Row, Col } from "antd";
import { connect } from "react-redux";
import { changeNationality } from "../Redux/Actions/SettingsActions";
import NationalitySettings from "../Components/Settings/NationalitySettings";

class SettingsContainer extends Component {
  render() {
    return (
      <Row>
        <Col>
          <h1 style={{ marginTop: "15px" }}>Settings</h1>
          <NationalitySettings
            onChange={this.props.handleChangeNationality}
            nationalities={this.props.user.nationalities}
          />
        </Col>
      </Row>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user
});
const mapDispatchToProps = dispatch => {
  return {
    handleChangeNationality: nat => dispatch(changeNationality(nat))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SettingsContainer);
