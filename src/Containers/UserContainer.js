import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { Button, Affix, Row, Col } from "antd";
import { connect } from "react-redux";
import UserGrid from "../Components/User/UserGrid";
import UserDetailModal from "../Components/User/UserDetailModal";
import UserSearch from "../Components/User/UserSearch";
import {
  getUsersData,
  showUserDetail,
  closeUserDetail,
  searchUser
} from "../Redux/Actions/UserActions";
import { logoutUser } from "../Redux/Actions/AuthActions";
class UserContainer extends Component {
  componentDidMount() {
    this.props.getUserData();
    this.handleScroll();
  }
  handleScroll() {
    window.onscroll = async () => {
      if (
        this.props.user.isLoading ||
        !this.props.user.paginationInfo.hasMore()
      )
        return;
      if (
        window.innerHeight + document.documentElement.scrollTop ===
        document.documentElement.scrollHeight
      ) {
        await this.props.getUserData();
      }
    };
  }
  onLogout = e => {
    e.preventDefault();
    this.props.logoutUser();
    <Redirect to="/" />;
  };
  render() {
    return (
      <Row>
        <Col>
          <Row>
            <Col span={6}>
              <Affix style={{ marginTop: "10px" }}>
                <UserSearch onSearch={this.props.handleSearch} />
              </Affix>
            </Col>
            <Col>
              <Affix style={{ marginTop: "10px", float: "right" }}>
                <Button
                  onClick={this.onLogout}
                  type="primary"
                  icon="logout"
                  ghost
                >
                  Logout
                </Button>
              </Affix>
            </Col>
          </Row>
          <Row>
            {this.props.user.userDetail !== null ? (
              <UserDetailModal
                user={this.props.user.userDetail}
                onClose={this.props.closeUserDetail}
              />
            ) : null}
          </Row>
          <Row>
            <hr />
            <UserGrid
              user={this.props.user.filteredUsers}
              isLoading={this.props.user.isLoading}
              onUserClick={this.props.showUserDetail}
              hasMore={this.props.user.paginationInfo.hasMore()}
            />
          </Row>
        </Col>
      </Row>
    );
  }
}
const mapStateToProps = state => ({
  auth: state.auth,
  user: state.user
});
const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch(logoutUser()),
    getUserData: () => dispatch(getUsersData()),
    showUserDetail: data => dispatch(showUserDetail(data)),
    closeUserDetail: () => dispatch(closeUserDetail()),
    handleSearch: value => dispatch(searchUser(value))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);
