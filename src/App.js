import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Layout } from "antd";
import loadable from "@loadable/component";
import ProtectedRoutes from "./Components/ProtectedRoute/ProtectedRoutes";
const AppHeader = loadable(() => import("./Components/Common/AppHeader"));
const LoginContainer = loadable(() => import("./Containers/LoginContainer"));
const RegisterContainer = loadable(() =>
  import("./Containers/RegisterContainer")
);
const UserContainer = loadable(() => import("./Containers/UserContainer"));
const SettingsContainer = loadable(() =>
  import("./Containers/SettingsContainer")
);
const AppFooter = loadable(() => import("./Components/Common/AppFooter"));
import { Provider } from "react-redux";
import store from "./Redux/Store/Store";
import { loginSuccess, logoutUser } from "./Redux/Actions/AuthActions";
const { Header, Footer, Content } = Layout;
const token = localStorage.getItem("jwtToken");
if (localStorage.getItem("jwtToken")) {
  store.dispatch(loginSuccess(token));
  if (!token) {
    store.dispatch(logoutUser());
    window.location.href = "./";
  }
}
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Layout>
            <Header>
              <AppHeader />
            </Header>
            <Content
              style={{
                height: "100%",
                padding: "0 80px",
                background: "#ffffff  "
              }}
            >
              <Switch>
                <Route exact path="/" component={LoginContainer} />
                <Route exact path="/register" component={RegisterContainer} />
                <ProtectedRoutes exact path="/home" component={UserContainer} />
                <ProtectedRoutes
                  exact
                  path="/settings"
                  component={SettingsContainer}
                />
              </Switch>
            </Content>
            <Footer
              style={{
                background: "#011529",
                color: "#ffffff"
              }}
            >
              <AppFooter />
            </Footer>
          </Layout>
        </Router>
      </Provider>
    );
  }
}

export default App;
