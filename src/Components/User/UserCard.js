import React, { Component } from "react";
import { Card, Button, Avatar } from "antd";

const { Meta } = Card;

class UserCard extends Component {
  render() {
    const { thumbnail, userName, firstName, lastName, email } = this.props.user;
    return (
      <Card
        hoverable
        bordered={true}
        cover
        actions={[
          <Button
            onClick={() => this.props.onClick(this.props.user)}
            type="primary"
            icon="idcard"
            ghost
          >
            View Detail
          </Button>
        ]}
        style={{ marginTop: "10px", marginBottom: "10px" }}
      >
        <Meta
          avatar={<Avatar src={thumbnail} />}
          title={userName}
          description={
            <div>
              <p>
                <b>Name:</b>
                {firstName} {lastName}
              </p>
              <p>
                <b>Email:</b> {email}
              </p>
            </div>
          }
        />
      </Card>
    );
  }
}

export default UserCard;
