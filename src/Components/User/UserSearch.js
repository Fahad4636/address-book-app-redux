import React, { Component } from "react";
import { Input } from "antd";

const Search = Input.Search;

class UserSearch extends Component {
  render() {
    return (
      <Search
        placeholder="Search By User first & last name"
        enterButton
        allowClear={true}
        onSearch={this.props.onSearch}
      />
    );
  }
}
export default UserSearch;
