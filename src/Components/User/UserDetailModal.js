import React, { Component } from "react";
import { Modal, Avatar } from "antd";

class UserDetailModal extends Component {
  render() {
    const {
      thumbnail,
      userName,
      firstName,
      lastName,
      email,
      phone,
      street,
      state,
      postcode,
      city,
      cell
    } = this.props.user;
    return (
      <Modal
        title="User Detail"
        visible={true}
        onCancel={this.props.onClose}
        footer={false}
      >
        <div>
          <p>
            <Avatar src={thumbnail} alt="thumbnail" />
          </p>
          <p>
            <b>Username:</b> {userName}
          </p>
          <p>
            <b> Name:</b> {firstName} {lastName}
          </p>
          <p>
            <b>Email:</b> {email}
          </p>
          <p>
            <b> Location:</b>
            {street.number}:{street.name},{state}, {city}, {postcode}
          </p>
          <p>
            <b>Phone:</b> {phone}
          </p>
          <p>
            <b>Cell:</b> {cell}
          </p>
        </div>
      </Modal>
    );
  }
}
export default UserDetailModal;
