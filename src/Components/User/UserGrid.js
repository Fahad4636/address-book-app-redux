import React, { Component } from "react";
import { Row, Col, Spin, Typography } from "antd";
import UserCard from "./UserCard";
const { Text } = Typography;
class UserGrid extends Component {
  render() {
    const { user, isLoading, onUserClick, hasMore } = this.props;
    const dataList = user.map(users => (
      <Col key={users.userId} span={12}>
        <UserCard user={users} onClick={onUserClick} />
      </Col>
    ));
    return (
      <div>
        <Row gutter={1}>{dataList}</Row>
        <Row type="flex" justify="center">
          <Col span={2}>{isLoading ? <Spin size="large" /> : null}</Col>
        </Row>
        <Row type="flex" justify="center">
          <Col span={2}>
            {!hasMore ? (
              <Text type="warning" underline strong>
                End of User Catalog!
              </Text>
            ) : null}
          </Col>
        </Row>
      </div>
    );
  }
}

export default UserGrid;
