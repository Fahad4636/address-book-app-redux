import React, { Component } from "react";
import { NavLink, withRouter } from "react-router-dom";
import { Menu, Icon } from "antd";

class AppHeader extends Component {
  render() {
    return (
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={["/"]}
        selectedKeys={[location.pathname]}
        style={{ lineHeight: "64px" }}
      >
        <Menu.Item>
          <span>Address Book App</span>
        </Menu.Item>
        <Menu.Item key="/home">
          <NavLink to="/home">
            <Icon type="home" />
            <span>Home</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/settings">
          <NavLink to="/Settings">
            <Icon type="setting" />
            <span>Settings</span>
          </NavLink>
        </Menu.Item>

        <Menu.Item key="/register" style={{ float: "right" }}>
          <NavLink to="/register">
            <Icon type="plus-circle" />
            <span>Register</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key="/" style={{ float: "right" }}>
          <NavLink to="/">
            <Icon type="login" />
            <span>Login</span>
          </NavLink>
        </Menu.Item>
      </Menu>
    );
  }
}
export default withRouter(AppHeader);
