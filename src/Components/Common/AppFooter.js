import React, { Component } from "react";

class AppFooter extends Component {
  render() {
    return (
      <div style={{ textAlign: "center" }}>
        Address Book ©2019 Created by Webpack, React.js,Redux,Ant Design
      </div>
    );
  }
}
export default AppFooter;
