import React, { Component } from "react";
import { Select } from "antd";

const { Option } = Select;
const optionsList = ["CH", "ES", "FR", "GB"];
class NationalitySettings extends Component {
  render() {
    const nationalityOptions = optionsList.map((options, k) => {
      return <Option key={k}>{options}</Option>;
    });
    return (
      <Select
        mode="multiple"
        placeholder="Select Nationality"
        defaultValue={this.props.nationalities}
        onChange={this.props.onChange}
        style={{ width: "80%", marginBottom: "40px" }}
      >
        {nationalityOptions}
      </Select>
    );
  }
}
export default NationalitySettings;
